const Hapi = require('@hapi/hapi');
const Path = require('path');
const Joi = require('joi');

let users = []

const init = async () => {
    const server = Hapi.server({
        port: 3000,
        host: '0.0.0.0',
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'static'),
            },
        },
    });

    await server.register(require('@hapi/inert'));

    // Register a new username
    server.route({
        method: 'POST',
        path: '/login',
        handler: (request, h) => {
            const { username } = request.payload

            users[username] = {
                username
            }

            return h.response().redirect(`/onboard.html?username=${username}`);
        },
        options: {
            auth: false,
            validate: {
                payload: Joi.object({
                    username: Joi.string().min(1)
                }),
            },
        },
    });

    server.route({
        method: 'POST',
        path: '/user/onboard/complete',
        handler: (request, h) => {
            const {
                username,

                id, // base64 encoded binary blob
                rawId, // base64 encoded binary blob

                clientDataJSON, // base64 encoded json:
                // {
                //   "challenge": "aGVsbG8gd29ybGQ",
                //   "origin": "https://id.ddenis.info",
                //   "type": "webauthn.create"
                // }

                attestationObject, // base64 encoded CBOR payload:
                // {
                //   "authData": bytes // public key data
                //   "fmt": "apple"
                //   "attStmt": AppleStmtFormat { "x5c": [ credCert: bytes, caCert: bytes ] }
                // }
            } = request.payload;

            console.log(request.payload);

            const data = {
                redirectUrl: `webauthtestapp://auth-success/${username}?credentialId=${rawId}`,
            };
            return h.response(data);
        },
    });

    // validate public key credential when logging in with platform authentication
    server.route({
        method: 'POST',
        path: '/user/validate',
        handler: (request, h) => {
            const payload = request.payload;
            console.log(payload);

            const userId = new Buffer(payload.userHandle, 'base64').toString('ascii');
            const data = {
                redirectUrl: `webauthtestapp://auth-success/${userId}`,
            };
            return h.response(data);
        },
    });

    server.route({
        method: 'GET',
        path: '/debug/users',
        handler: function (request, h) {
            return h.response(Object.values(users).map(u => u.username));
        }
    });

    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: Path.join(__dirname, 'static'),
            }
        }
    });

    server.events.on('response', function (request) {
        console.log(request.info.remoteAddress + ': ' + request.method.toUpperCase() + ' ' + request.path + ' --> ' + request.response.statusCode);
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();
