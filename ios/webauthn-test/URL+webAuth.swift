//
//  URL+webAuth.swift
//  webauthn-test
//
//  Created by Denis Dzyubenko on 02/09/2020.
//

import Foundation

extension URL {
    struct WebAuth {
        let userId: String
    }
    var authUrl: WebAuth? {
        guard scheme == "webauthtestapp" else { return nil }
        guard host == "auth-success" else {
            assertionFailure()
            return nil
        }
        return WebAuth(userId: lastPathComponent)
    }
}
