//
//  SceneDelegate.swift
//  webauthn-test
//
//  Created by Denis Dzyubenko on 01/09/2020.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }

        // TODO: handle opened urls
    }

    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        print("### open url: \(URLContexts)")
        let viewController = window?.rootViewController as? ViewController
        viewController?.didOpenUrl(URLContexts.first!.url)
    }
}

