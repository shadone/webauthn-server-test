//
//  ViewController.swift
//  webauthn-test
//
//  Created by Denis Dzyubenko on 01/09/2020.
//

import UIKit
import AuthenticationServices
import SafariServices
import DeviceCheck
import CryptoKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func signInWithASWebAuthenticationSession(_ sender: Any) {
        let authURL = URL(string: "https://id.ddenis.info/")!
        let scheme = "webauthtestapp://"

        // Initialize the session.
        let session = ASWebAuthenticationSession(
            url: authURL, callbackURLScheme: scheme) { [weak self] callbackURL, error in
            // Handle the callback.
            if let error = error as? ASWebAuthenticationSessionError {
                switch error.code {
                case .canceledLogin:
                    return
                case .presentationContextInvalid:
                    assertionFailure()
                case .presentationContextNotProvided:
                    assertionFailure()
                @unknown default:
                    print("Error: \(error)")
                }
                return
            } else if let error = error {
                print("Error: \(error)")
            }

            if let callbackURL = callbackURL {
                print("Success: \(callbackURL)")
                guard let authResult = callbackURL.authUrl else {
                    assertionFailure()
                    return
                }
                self?.didLogIn(authResult)
            }
        }

        session.presentationContextProvider = self

        session.start()
    }

    func didLogIn(_ result: URL.WebAuth) {
        let alert = UIAlertController()
        alert.title = "Auth succeeded"
        alert.message = "Authenticated as '\(result.userId)'"
        alert.addAction(.init(title: "OK", style: .default, handler: nil))
        present(alert, animated: true)
    }

    var safariViewController: SFSafariViewController?

    @IBAction func signInWithSFSafariViewController(_ sender: Any) {
        let authURL = URL(string: "https://id.ddenis.info/")!
        let config = SFSafariViewController.Configuration()
        let vc = SFSafariViewController(url: authURL, configuration: config)
        safariViewController = vc
        present(vc, animated: true)
    }

    @IBAction func signInWithSFSafariViewControllerWithCredential(_ sender: Any) {
        var components = URLComponents(string: "https://id.ddenis.info/signin.html")!
        components.queryItems = [
            URLQueryItem(name: "credentialId", value: "DDJ3pN1/sajRjZD6YoK6QpLf5jI=".replacingOccurrences(of: "+", with: "%2B"))
        ]
        let authURL = components.url!
        let config = SFSafariViewController.Configuration()
        let vc = SFSafariViewController(url: authURL, configuration: config)
        safariViewController = vc
        present(vc, animated: true)
    }

    @IBAction func generateKey(_ sender: Any) {
        DCAppAttestService.shared.generateKey { (keyId, error) in
            if let error = error {
                print("#### \(error)")
                return
            } else if let keyId = keyId {
                print("Generated key: \(keyId)")
                UserDefaults.standard.set(keyId, forKey: "keyId")
            } else {
                assertionFailure()
            }
        }
    }

    @IBAction func attestKey(_ sender: Any) {
        guard let keyId = UserDefaults.standard.string(forKey: "keyId") else {
            print("### no key")
            return
        }

        let challenge = "hello world".data(using: .utf8)!
        let hash = Data(SHA256.hash(data: challenge))

        DCAppAttestService.shared.attestKey(keyId, clientDataHash: hash) { (attestationObject, error) in
            if let error = error {
                print("#### \(error)")
            } else if let attestationObject = attestationObject {
                print("Attested key \(keyId): \(attestationObject.base64EncodedString(options: []))")
            } else {
                assertionFailure()
            }
        }
    }

    @IBAction func generateAssertion(_ sender: Any) {
        guard let keyId = UserDefaults.standard.string(forKey: "keyId") else {
            print("### no key")
            return
        }

        let challenge = "hello world".data(using: .utf8)!
        let hash = Data(SHA256.hash(data: challenge))

        DCAppAttestService.shared.generateAssertion(keyId, clientDataHash: hash) { (assertionObject, error) in
            if let error = error {
                print("#### \(error)")
            } else if let assertionObject = assertionObject {
                print("Asserion for key \(keyId): \(assertionObject.base64EncodedString(options: []))")
            } else {
                assertionFailure()
            }
        }
    }

    func didOpenUrl(_ url: URL) {
        safariViewController?.dismiss(animated: true, completion: { [weak self] in
            guard let authResult = url.authUrl else {
                assertionFailure()
                return
            }
            self?.didLogIn(authResult)
        })
        safariViewController = nil
    }
}

extension ViewController: ASWebAuthenticationPresentationContextProviding {
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        return view.window!
    }
}
